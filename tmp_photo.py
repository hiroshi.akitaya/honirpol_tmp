#!/usr/bin/env python3

#
#  Tempolarl photometry and polarimetry module.
#    tmp_photo.py
#
#   2023/10/16 H. Akitaya
#

import os
import sys
import glob


import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits
from astropy.table import Table
from astropy.table import vstack
from astropy.io import ascii

from photutils.aperture import CircularAnnulus, CircularAperture
from photutils.aperture import ApertureStats
from photutils.aperture import aperture_photometry
from astropy.stats import SigmaClip

from astropy.stats import sigma_clipped_stats
from photutils.detection import find_peaks

# HONIR polcal
from polcal import ImpolData
import polcal


def get_flat_fn(obsdate):
    flat_dir = '/home/akitaya/d5/honir_impol/flat/'
    #if datetime.strptime(obsdate, "%Y-%m-%d") < datetime.strptime('2023-02-03', "%Y-%m-%d"):
    #    return(os.path.join(flat_dir, "2023-01-22/flat_opt_ImPol_R_partial.fits"))
    if datetime.strptime(obsdate, "%Y-%m-%d") < datetime.strptime('2023-05-01', "%Y-%m-%d"):
        return(os.path.join(flat_dir, "2023-02-17/flat_opt_ImPol_R_partial.fits"))
    else:
        return(os.path.join(flat_dir, "2023-05-12/flat_opt_ImPol_R_partial.fits"))


def show_all_headers(fn):
    """ Show all fits header information.
    """
    with fits.open(fn) as hdul:
        for key in hdul[0].header:
            print(key, hdul[0].header[key])


def select_images_by_header(fns, cond_dict, logic='or'):
    """ Select filenames by fits header values.
    :param fns: file name list to be seached.
    :param cond_dict: Dictionary of a pair of header_key and value.
    :return: file name list.
    """
    fns_result = []
    for fn in fns:
        with fits.open(fn) as hdul:
            if logic == 'or':
                for key in cond_dict:
                    if hdul[0].header[key] == cond_dict[key]:
                        fns_result.append(fn)
            else:
                flag = True
                for key in cond_dict:
                    if hdul[0].header[key] != cond_dict[key]:
                        flag = False
                if flag:
                    fns_result.append(fn)
    return fns_result


def bias_subtraction(fn: str, fn_bias: str, overwrite=False, subext='_bs'):
    """ Bias subtraction.
    """
    with fits.open(fn) as hdul_img:
        with fits.open(fn_bias) as hdul_bias:
            fn_splt = os.path.splitext(fn)
            fn_new = fn_splt[0] + subext + fn_splt[1]
            hdul_img[0].data -= hdul_bias[0].data
            # Comments
            hdul_img[0].header['history'] = f'Bias subtraction: {fn} - {fn_bias} = {fn_new}'
            hdul_img.writeto(fn_new, overwrite=overwrite)
            return fn_new
    return None


def flat_fielding(fn: str, fn_flat: str, overwrite=False, subext='_fl'):
    """ Flat fielding.
    """
    with fits.open(fn) as hdul_img:
        with fits.open(fn_flat) as hdul_flat:
            fn_splt = os.path.splitext(fn)
            fn_new = fn_splt[0] + subext + fn_splt[1]
            hdul_img[0].data /= hdul_flat[0].data
            # Comments
            hdul_img[0].header['history'] = f'Flat fielding: {fn} / {fn_flat} = {fn_new}'
            hdul_img.writeto(fn_new, overwrite=overwrite)
            return fn_new
    return None


def imgs_combine(fns, fn_out=None, overwrite=True):
    """ Combine fits images.
    median mode.
    """
    imgs = []
    for fn in fns:
        with fits.open(fn) as hdul:
            imgs.append(hdul[0].data)
            print(fn)
    if len(imgs) < 1:
        sys.stderr.write('No images found.\n')
        sys.exit(1)
    imgs_ndarray = np.stack(imgs)
    del(imgs)
    imgs_comb = np.median(imgs_ndarray, axis=0)
    hdul[0].data = imgs_comb
    hdul[0].header['history'] = f'Combined: {str(fns)}'
    hdul[0].header['history'] = f'Combined file name: {fn}'
    if fn_out:
        hdul.writeto(fn_out, overwrite=overwrite)
    print(f'Write to {fn_out}')
    del(imgs_ndarray)
    return(hdul)


def preproc(obs_date_str, overwrite=True):
    if not os.path.isdir(obs_date_str):
        return False
    os.chdir(obs_date_str)
    
    fns = glob.glob('*opt00.bs.fits')
    fns.sort()

    # Bias combination.
    fn_bias = 'bias_comb.fits'
    fns_bias = select_images_by_header(fns, {'DATA-TYP': 'BIAS'})
    hdul = imgs_combine(fns_bias, fn_out=fn_bias)

    # Bias subtraction.
    fns_obj = select_images_by_header(fns, {'DATA-TYP': 'STANDARD'})
    fns_bsub = []
    for fn in fns_obj:
        fns_bsub.append(bias_subtraction(fn, fn_bias, overwrite=True))

    # Flat fielding.
    flat_date_str = fits.getheader(fns[0])['DATE-OBS']
    flat_fn = get_flat_fn(flat_date_str)
    fns_flat = []
    for fn in fns_bsub:
        fns_flat.append(flat_fielding(fn, flat_fn, overwrite=True))

    os.chdir('..')
    
    return fns_flat





def copy_file(fn, objname, dirbase, dirname, verbose=False):
    """Copy fits file with the same objname. """
    if os.path.exists(dirname) and not os.path.isdir(dirname):
        sys.stderr.write(f'Some file with a name {dirname} exists.\n')
        return False
    else:
        if not os.path.exists(dirname):
            os.mkdir(dirname)
            print(f'Create directory {dirname}')
    with fits.open(fn) as hdul:
        try:
            objname_in_file = hdul[0].header['OBJECT']
        except KeyError:
            sys.stderr.write('OBJECT header not found. Skip.\n')
            return False
        if verbose: print(fn, objname_in_file)
        if hdul[0].header['OBJECT'] != objname:
            return False
        else:
            newpath = shutil.copy2(fn, dirname)
            print(f'File copy: {newpath}')
    return True


def copy_files(fns, objname, dirbase, dirname, verbose=False):
    fns = glob.glob(os.path.join(dirbase, dirname, '*opt*.fits'))
    fns.sort()
    n_files = 0
    for fn in fns:
        if copy_file(fn, objname, dirbase, dirname, verbose):
            n_files += 1
    print(f'Total {n_files} files copied.')
    return n_files, objname, dirname


def get_gain(hdul):
    try:
        return hdul[0].header['GAIN']
    except ValueError:
        return None


def apphot_pol_oe(hdul, coords, r_ap=12, r_in=15, r_out=18, 
                  bg_sig=3.0, bg_iter=10, warning=True):
    SATURATED_LEVEL = 60000.0
    
    # Check apertures.
    if len(coords) !=2:
        sys.stderr.write('Number of apertures should be 2. Abort.\n')
        return None

    data = hdul[0].data

    # Aperture photometry.
    apers = CircularAperture(coords, r_ap)
    aperstats = ApertureStats(data, apers)
    columns = ('id', 'xcentroid', 'ycentroid', 'sum_aper_area', 
               'max', 'min', 'mean', 'median', 'std', 'var', 'sum', 'fwhm')
    stats_tbl = aperstats.to_table(columns)
    if warning:
        if stats_tbl['max'][0] > SATURATED_LEVEL:
            print(f'Maximum count is close to the SATURATED LEVEL ({SATURATED_LEVEL})')

    # Background.
    annulus_aperture = CircularAnnulus(coords, r_in=r_in, r_out=r_out)
    sigclip = SigmaClip(sigma=bg_sig, maxiters=bg_iter)
    bkgstats = ApertureStats(data, annulus_aperture, sigma_clip=sigclip)
    total_bkg = bkgstats.median * aperstats.sum_aper_area.value
    apersum_bkgsub = aperstats.sum - total_bkg
    stats_tbl.add_column(total_bkg, name='bkg_total')
    stats_tbl.add_column(apersum_bkgsub, name='sum_bkgsub')

    # Error calculation.
    gain = get_gain(hdul)
    ph_error = np.sqrt(stats_tbl['sum'] * gain + stats_tbl['bkg_total'] * gain) / gain  # Ignore readout noise.
    stats_tbl.add_column(ph_error, name='apphot_error')  # Photometry error.
    stats_tbl.add_column(stats_tbl['sum_bkgsub'] / stats_tbl['apphot_error'], name='sn_ratio') # SN ratio.

    # Meta information.
    stats_tbl.add_column(('o', 'e'), name='ray')
    stats_tbl.add_column((os.path.basename(hdul[0].header['FRAMEID']),)*2, name='filename')
    stats_tbl.add_column((hdul[0].header['FILTER02'],)*2, name='band')
    stats_tbl.add_column((hdul[0].header['HWPANGLE'],)*2, name='hwp')

    return stats_tbl


def shape_oe_table(tbl_in):
    if len(tbl_in) != 2:
        sys.stderr.write('Number of rows in the table should be 2. Abort.\n')
        return None
    fn = tbl_in[tbl_in['ray']=='o']['filename']
    hwp = tbl_in[tbl_in['ray']=='o']['hwp']
    band = tbl_in[tbl_in['ray']=='o']['band']
    io = tbl_in[tbl_in['ray']=='o']['sum_bkgsub']
    ie = tbl_in[tbl_in['ray']=='e']['sum_bkgsub']
    io_err = tbl_in[tbl_in['ray']=='o']['apphot_error']
    ie_err = tbl_in[tbl_in['ray']=='e']['apphot_error']
    
    tbl_out = Table([fn, hwp, band, io, io_err, ie, ie_err], 
                    names=('filename', 'hwp', 'band', 'io', 'io_err', 'ie', 'ie_err')
                   )
    return tbl_out


def get_oe_coords_from_o_coord(coord_o: list):
    if len(coord_o) != 2:
        return None
    coord_e = (coord_o[0] + 165, coord_o[1])
    return (coord_o, coord_e)


def impol(fns_fl, coords, band, r_ap=12, r_in=15, r_out=18):
    tbls = []
    for fn in fns_fl:
        print(fn)
        with fits.open(fn) as hdul:

            # calc. centroid.
            coord_o = coords[0]
            mean, median, std = sigma_clipped_stats(hdul[0].data, sigma=3.0)
            threshold = median + (5.0 * std)
            
            mask = np.ones(hdul[0].data.shape, dtype=bool)
            mask_o = unmask_part(coord_o, mask, 40)
            tbl = find_peaks(hdul[0].data, threshold, box_size=21, mask=mask_o)
            if tbl is None:
                continue
            tbl['peak_value'].info.format = '%.8g' 
            coord_o_center = (tbl[0]['x_peak'], tbl[0]['y_peak'])
            coords_new = get_oe_coords_from_o_coord(coord_o_center)
            #print(fn)  # Debug.
            tbl1 = apphot_pol_oe(hdul, coords_new, r_ap=r_ap, r_in=r_in, r_out=r_out)
            tbl2 = shape_oe_table(tbl1)
            tbls.append(tbl2)
    tbl_all = vstack(tbls)
    del(tbls)
    print(tbl_all)
    impoldata = ImpolData(fn=None, tbl_oe=tbl_all)
    impoldata.set_band(band)
    result = impoldata.calc_pol()
    #print(len(result))
    #tbl_result = Table(result, 
    #                   names=('band', 'wavelength', 'q_ave', 'q_obserr', 'q_staterr', 
    #                           'u_ave', 'u_obserr', 'u_staterr',
    #                           'p', 'p_obserr', 'p_staterr', 't', 't_obserr', 't_staterr', 
    #                           'q_n', 'u_n'),
    #                   dtype=('U1','f8','f8','f8','f8','f8','f8','f8','f8','f8','f8','f8','f8','f8','i4','i4'))
    return result


def unmask_part(coord, mask, boxsize):
    hw = int(boxsize/2)
    x0 = coord[0]
    y0 = coord[1]
    mask[y0-hw:y0+hw-1, x0-hw:x0+hw-1] = False
    return mask


def easy_imshow(fn):
    imgdata = fits.getdata(fn)
    plt.imshow(imgdata, vmin=0, vmax = np.median(imgdata)*3.0)


